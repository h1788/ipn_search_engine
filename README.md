# IPN
Knowledge hub for IPN that aggregates content from nearly 50 different sources around IPN. 



## Getting started
To access an app, just navigate to the URL: `http://ipnhub.bards.ai`. If you want to build / start the app on your own machine, you can follow the instructions below:


### Preparing Backend
First you need to generate Google API Credentials for Google Custom Search Engine. Put them in  `backend/.env` file.

Then you need need to install requirements from backend/requirements.txt file. Then you can start with uvicorn, eg.:
```bash
uvicorn main:app --host 0.0.0.0 --port 3777
```

### Preparing frontend
Change `BACKEND_URL` in `frontend/src/utils/backend.jsx` to your an adress, so that frontend on client machine can reach backend url. If you are running whole stack on your localmachine, you can set it as `http://localhost:3777`

Then just cd to frontend and run `npm install` then `npm run start`. You can access the application on `http://localhost:3000`.