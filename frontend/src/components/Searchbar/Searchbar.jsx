import * as React from 'react';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import DirectionsIcon from '@mui/icons-material/Directions';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import styles from './Searchbar.module.css';
import autocomplete from '../../api/autoComplete';
import { Link } from 'react-router-dom';

export default function SearchBar(props) {

  let [query, setQuery] = React.useState('');
  let [suggestions, setSuggestions] = React.useState([]);

  React.useEffect(() => {
    autocomplete(query, setSuggestions);
  }, [query]);

  let text = {
    pl: {
      placeholder: "Przeszukaj zasoby instytutu pamięci narodowej..."
    },
    en: {
      placeholder: "Search IPN resources..."
    }
  }[props.lang];

  return (
    <Paper
      component="form"
      className={styles.search_outline}
      sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 800 }}
    >
      <IconButton sx={{ p: '10px' }} aria-label="menu">
        <MenuIcon />
      </IconButton>
      <Autocomplete
        options={suggestions}
        filterOptions={(x) => x} 
        freeSolo
        disableClearable
        inputValue={query}
        onInputChange={(event, newInputValue) => {
          setQuery(newInputValue);
        }}
        id="combo-box-demo"
        className={styles.searchinput}
        renderInput={(params) => (<TextField
          {...params}
          sx={{ ml: 1, flex: 1 }}
          placeholder={text.placeholder}
          InputProps={{
            ...params.InputProps,
            type: 'search',
          }}
        />)}
      />
      
      <Link to="/search" state={{query: query}}>
        <IconButton type="submit" sx={{ p: '10px' }} aria-label="search">
          <SearchIcon />
        </IconButton>
      </Link>
    </Paper>
  );
}