import IPNAppBar from '../Appbar/appbar';
import SearchResultSearchbar from '../SearchResultSearchbar/SearchResultSearchbar';
import Container from '@mui/material/Container';
import styles from './ArticlePage.module.css';
import Typography from '@mui/material/Typography';
import SearchResultElement from '../SearchResultElement/SearchResultElement';
import scrape from '../../api/article';
import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import Grid from '@mui/material/Grid';
import RecomendationEntry from './RecomendationVeritical';
import IconButton from '@mui/material/IconButton';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from 'react-use';

function shuffle(array) {
  let currentIndex = array.length, randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex != 0) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

export default function ArticlePage(props) {
  let location = useLocation();
  let article_url = location.state.url;
  let articles = location.state.articles;
  let articles_items = shuffle(articles.items);

  let text = {
    pl: {
      similar: "Podobne",
      source: "Źródło",
    },
    en: {
      similar: "Similar",
      source: "Source",
    }
  }[props.lang]

  let [scrapeData, SetScrapeData] = useState({
    date: "",
    title: "",
    text: [],
  });

  useEffect(() => {
    scrape(article_url, SetScrapeData);
  }, [article_url]);

  return (
    <div>
      <IPNAppBar setLogged={props.setLogged} logged={props.logged} lang={props.lang} setLang={props.setLang} />
      <Container maxWidth='lg' className={styles.container}>
        <Grid container spacing={3}>
          <Grid item xs={8}>
            <Typography className={styles.results_header} variant="subtitle1" gutterBottom component="div">
              {scrapeData.date}
            </Typography>
            <Typography className={styles.results_header} variant="h3" gutterBottom component="h2">
              {scrapeData.title}
            </Typography>
            <div className={styles.moreinfo}>
              <IconButton color="primary" aria-label="upload picture" component="span" onClick={() => window.print()}>
                <LocalPrintshopOutlinedIcon />
              </IconButton>
              <Typography variant="subtitle1" component="div">
                <a href={article_url}>{text.source}</a>
              </Typography>
            </div>
            <Typography className={styles.results_header} variant="body" gutterBottom component="p">
              {scrapeData.text.map(text => <p>{text}</p>)}
            </Typography>
            <Typography className={styles.results_Link} variant="subtitle1" gutterBottom component="div">
              {text.source}: <a href={article_url}>{article_url}</a>
            </Typography>
            <Typography className={styles.author} variant="subtitle2" gutterBottom component="div">
              {scrapeData.author}
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <Typography className={styles.results_header} variant="h4" gutterBottom component="h4">
              {text.similar}
            </Typography>
            {articles_items.slice(0, 3).map(i => <RecomendationEntry url={i.link} articles={articles} text={i.text} image={i.image.length > 0 ? i.image : "/images/ipn.jpg"} title={i.title} />)}
          </Grid>
        </Grid>
      </Container>
    </div>

  );
}
