import styles from './login.module.css';

export default function Login(props) {
    return <a onClick={props.setLogged(true)} href="/"><img className={styles.logowanie} src="/images/logowanie.png"></img></a>
}