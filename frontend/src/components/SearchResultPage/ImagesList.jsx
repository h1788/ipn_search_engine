import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import ListSubheader from '@mui/material/ListSubheader';
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';

export default function ImagesList(props) {
    console.log(props.images.items)
    return (
        <ImageList>
            <ImageListItem key="Subheader" cols={5}>
                <ListSubheader component="div"></ListSubheader>
            </ImageListItem>
            {props.images.items.map((item) => (
                <ImageListItem key={item.link}>
                    <img
                        src={`${item.link}`}
                        loading="lazy"
                    />
                    <ImageListItemBar
                        title={item.title}
                        subtitle={item.site_domain}
                        actionIcon={
                            <IconButton
                                sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                            >
                                <InfoIcon />
                            </IconButton>
                        }
                    />
                </ImageListItem>
            ))}
        </ImageList>
    );
}