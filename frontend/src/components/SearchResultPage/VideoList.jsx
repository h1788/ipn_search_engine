import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import search_video from '../../api/search_video';

export default function VideoList(params) {

    return (
        <Grid container spacing={2}>
            {params.videos.items.map((video) => (
                <Grid item xs={4}>
                    <Card>
                        <CardActionArea>
                            <CardMedia
                                component="iframe"
                                frameBorder="0"
                                height="250"
                                src={video.src}
                            />
                        </CardActionArea>
                    </Card>
                </Grid>
            ))}
        </Grid>
    )
}