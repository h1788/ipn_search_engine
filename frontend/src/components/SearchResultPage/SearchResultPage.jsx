import IPNAppBar from '../Appbar/appbar';
import SearchResultSearchbar from '../SearchResultSearchbar/SearchResultSearchbar';
import Container from '@mui/material/Container';
import styles from './SearchResultPage.module.css';
import Typography from '@mui/material/Typography';
import SearchResultElement from '../SearchResultElement/SearchResultElement';
import search from '../../api/search';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Pagination from '@mui/material/Pagination';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import { Link } from 'react-router-dom';
import Recomendations from '../Recomendations/Recomendations';

import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { style } from '@mui/system';
import Grid from '@mui/material/Grid';
import image_search from '../../api/search_images';
import related from '../../api/related';
import ImagesList from './ImagesList';
import VideoList from './VideoList';
import search_video from '../../api/search_video';

export default function SearchResultPage(props) {
  let location = useLocation();
  const [tab, setTab] = useState(0);
  let query = location.state.query;
  let [searchResults, SetSearchResults] = useState({
    'items': [],
    'query': '',
    total_results: 0,
  });

  let [searchPDFResults, SetSearchPDFResults] = useState({
    'items': [],
    'query': '',
    total_results: 0,
  });

  let [page, setPage] = useState(1);

  let [searchImagesResults, SetSearchImagesResults] = useState({
    'items': [],
    'query': '',
    total_results: 0,
  });

  let [relatedResults, setRelatedResults] = useState({
    'items': [],
    'related_queries': [],
  });

  useEffect(() => {
    search(query, SetSearchResults, page);
    search(query, SetSearchPDFResults, page, 'pdf');
    image_search(query, SetSearchImagesResults)
    related(query, setRelatedResults)
  }, [query, page]);

  let [videos, setVideos] = useState({
    items: [],
    query: '',
  });

  useEffect(() => {
    search_video(query, setVideos);
  }, [query]);

  let text = {
    pl: {
      recomendations: 'Może cię zainteresować',
      popular: "Popularne teraz",
      your_results: "Twoje wyniki",
      video: "Nagrania",
      images: "Zdjęcia",
      pdf: "Pliki archiwalne",
      suggested: "Sugerowane zapytania",
      related_images: "Powiązane obrazy",
      phrase: "Wyniki wyszukiwania dla frazy",
      found: "Znaleziono",
      results: "wyników",
    },
    en: {
      recomendations: 'You may also like',
      popular: "Popular now",
      your_results: "Your results",
      video: "Videos",
      images: "Images",
      pdf: "Pdf files",
      suggested: "Suggested queries",
      related_images: "Related images",
      phrase: "Search results for phrase",
      found: "Found",
      results: "results"
    },
  }[props.lang]

  let results = (<div>
    <div>
      {searchResults.items.map(i => <SearchResultElement articles={searchResults} domain={i.site_domain} title={i.title} text={i.text} type={i.type} url={i.link} key={i} />)}
    </div>
  </div>);

  let results_pdf = (<div>
    <div>
      {searchPDFResults.items.map(i => <SearchResultElement articles={searchResults} domain={i.site_domain} title={i.title} text={i.text} type={i.type} url={i.link} key={i} />)}
    </div>
  </div>);

  let results_images = <ImagesList images={searchImagesResults} />
  let results_videos = <VideoList videos={videos} />

  let recomendations = (<div>
    <Recomendations title={text.recomendations} num={3} size={4} />
    <Recomendations title={text.popular} num={10} size={4} />
  </div>);


  return (
    <div>
      <IPNAppBar setLogged={props.setLogged} logged={props.logged} lang={props.lang} setLang={props.setLang} />
      <Container maxWidth='lg' className={styles.container}>
        <SearchResultSearchbar lang={props.lang} />
        <Typography className={styles.results_header} variant="h5" gutterBottom component="h2">
          {text.phrase} "<strong>{query}</strong>"
        </Typography>

        <Grid container spacing={3}>
          <Grid item xs={tab == 0 ? 8 : 12}>

            <Tabs className={styles.tabs} value={tab} onChange={(e, v) => setTab(v)} aria-label="basic tabs example">
              <Tab label={text.your_results} />
              <Tab label={text.video} />
              <Tab label={text.images} />
              <Tab label={text.pdf} />
            </Tabs>

            {(searchResults.items.length == 0) && recomendations}
            {(searchResults.items.length > 0 && tab == 0) && results}
            {(searchResults.items.length > 0 && tab == 2) && results_images}
            {(searchResults.items.length > 0 && tab == 1) && results_videos}
            {(searchResults.items.length > 0 && tab == 3) && results_pdf}
            <Typography className={styles.suggestions_header} variant="h4" gutterBottom component="h5">
              {text.suggested}
            </Typography>
            <div className={styles.suggestions}>
              {relatedResults.related_queries.map((i) => <Chip label={i} variant="outlined" />)}
            </div>
          </Grid>
          {(tab != 0) ? (<div></div>) : (<Grid item xs={3}>
            <Typography className={styles.results_header} variant="h5" gutterBottom component="h2">
              {text.related_images}
            </Typography>
            <ImageList variant="masonry" cols={3}>
              {searchImagesResults.items.map((item) => (
                <ImageListItem key={item.link}>
                  <img
                    src={item.link}
                    loading="lazy"
                  />
                </ImageListItem>
              ))}
            </ImageList>
          </Grid>)}
        </Grid>

        <Pagination page={page} onChange={(e, v) => setPage(v)} className={styles.pagination} count={10} size="large" color="primary" />
      </Container>
    </div>

  );
}
