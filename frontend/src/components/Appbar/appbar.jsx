import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import styles from './appbar.module.css';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';

import { useState } from 'react';

import { useNavigate } from 'react-router-dom';
import { useLocalStorage } from 'react-use';

export default function IPNAppBar(props) {
  let navigate = useNavigate();
  const [anchorEl, setAnchorEl] = useState(null);

  let handleMenu = e => setAnchorEl(e.currentTarget);
  let closeMenu = e => setAnchorEl(null);

  let texts = {
    pl: {
      profile: 'Profil',
      logout: 'Wyloguj',
      admin_panel: 'Panel administratora',
    },
    en: {
      profile: 'Profile',
      logout: 'Logout',
      admin_panel: 'Admin panel',
    },
  }[props.lang]

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }} onClick={() => navigate(-1)}>
            <ArrowBackOutlinedIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" component="div">
            IPN Hub
          </Typography>


          <div className={styles.login_menu}>
            <img onClick={() => props.setLang('pl')} className={styles.lang} src="/images/poland.png" style={{ height: 20 }}></img>
            <img onClick={() => props.setLang('en')} src="/images/england.png" style={{ height: 20 }}></img>
            {props.logged && <div>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={closeMenu}
              >
                <MenuItem onClick={closeMenu}>{texts.profile}</MenuItem>
                <Link to="/dashboard"><MenuItem onClick={closeMenu}>{texts.admin_panel}</MenuItem></Link>
                <MenuItem onClick={closeMenu} onClick={() => props.setLogged(false)}>{texts.logout}</MenuItem>
              </Menu>
            </div>}
            {!props.logged && <Button color="inherit" href='/login'>Login</Button>}
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
}