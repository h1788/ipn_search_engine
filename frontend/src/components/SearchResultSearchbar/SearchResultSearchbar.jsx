import SearchBar from "../Searchbar/Searchbar";
import styles from "./SearchResultSearchbar.module.css";

export default function SearchResultSearchbar(props) {
  return (
      <div className={styles.container}>
        <img className={styles.logo} src="/images/ipn.jpg" alt="IPN Logo" />
        <SearchBar lang={props.lang}/>
      </div>
  );
}
