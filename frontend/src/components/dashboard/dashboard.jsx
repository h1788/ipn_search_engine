import IPNAppBar from "../Appbar/appbar"
import styles from "./dashboard.module.css"
import Button from '@mui/material/Button';
import { Link } from "@mui/material";
import { useLocalStorage } from 'react-use';

export default function Dashboard(props) {
    let text = {
        pl: {
            download: "Pobierz Dane",
        },
        en: {
            download: "Download Data",
        }
    }[props.lang]

    return (
        <div className={styles.background}>
            <IPNAppBar setLogged={props.setLogged} logged={props.logged} lang={props.lang} setLang={props.setLang} />
            <div className={styles.container}>
                <Button href="/data.csv" className={styles.button} variant="outlined">{text.download}</Button>
                <img src="/images/dashboard.png" className={styles.dashboard}></img>
            </div>
        </div>
    )
}