import IPNAppBar from '../Appbar/appbar';
import Jumbo from '../Jumbo/Jumbo';
import Recomendations from '../Recomendations/Recomendations';
import Container from '@mui/material/Container';

export default function LandingPage(props) {
  let text = {
    pl: {
      recomended: "Specjalnie dla ciebie",
      popular: "Najpopularniejsze",
    },
    en: {
      recomended: "Recommended for you",
      popular: "Popular",
    },
  }[props.lang];

  return (
    <div>
      <IPNAppBar setLogged={props.setLogged} logged={props.logged} lang={props.lang} setLang={props.setLang} />
      <Jumbo lang={props.lang} />
      <Container maxWidth={false} >
        <Recomendations num={4} title={text.recomended} />
      </Container>
      <Container maxWidth={false} >
        <Recomendations num={4} title={text.popular} />
      </Container>
    </div>
  );
}
