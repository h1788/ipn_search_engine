import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import styles from './Jumbo.module.css';
import SearchBar from '../Searchbar/Searchbar';

export default function Jumbo(props) {
  return (
    <div className={styles.outer}>
        <div className={styles.inner}/>
        <div className={styles.content}>
          <img src="/images/ipn-transparent.png" alt="logo" className={styles.logo}/>
          <SearchBar lang={props.lang} />
        </div>
    </div>
  );
}