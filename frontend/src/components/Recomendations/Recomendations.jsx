import * as React from 'react';
import styles from './Recomendations.module.css';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import RecomendationCard from '../RecomendationCard/RecomendationCard';
import { useEffect } from 'react';
import popular from '../../api/popular';
import Grid from '@mui/material/Grid';

export default function Jumbo(props) {
  let [articles, setArticles] = React.useState({
    items: [],
  });

  useEffect(() => {
    popular(props.num, setArticles);
  }, [props.num]);

  let size = (props.size ? props.size : (12 / props.num));
  
  return (
    <div className={styles.container}>
      <Typography variant="h3" gutterBottom component="h2">
        {props.title}
      </Typography>
      <Grid container spacing={2} className={styles.recomendations}>
        {
          articles.items.map(i => <Grid item xs={size}> <RecomendationCard image={i.image.length > 0 ? i.image : "/images/ipn.jpg"} title={i.title} url={i.link} articles={articles} text={i.text} key={i.link} /></Grid>)
        }
      </Grid>
    </div>
  );
}