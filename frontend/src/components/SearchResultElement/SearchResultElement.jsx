import IPNAppBar from '../Appbar/appbar';
import SearchResultSearchbar from '../SearchResultSearchbar/SearchResultSearchbar';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import styles from './SearchResultElement.module.css';
import { Link } from 'react-router-dom';
import Badge from '@mui/material/Badge';
import Chip from '@mui/material/Chip';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';


export default function SearchResultElement(props) {
    if (props.type == "site") {
        return (
            <Link className={styles.link} to="/article" state={{url: props.url, articles: props.articles}} >
                <div className={styles.container}>
                    <Chip size="small" icon={<InfoOutlinedIcon />} label={props.domain} variant="outlined" />
                    <Typography variant="h6" gutterBottom component="h3">
                        {props.type == 'site' ? "" : ("[" + props.type + "]")} {props.title}
                    </Typography>

                    <Typography variant="body" gutterBottom component="p">
                        {props.text}
                    </Typography>
                </div>
            </Link>
        );
    } else {
        return (
            <a href={props.url} className={styles.link}>
                <div className={styles.container}>
                <Chip className={styles.chip} size="small" icon={<InfoOutlinedIcon />} label={props.domain} variant="outlined" />
                <Chip className={styles.chip} size="small" color='primary' icon={<InfoOutlinedIcon />} label={props.type} variant="outlined" />
                <Typography variant="h6" gutterBottom component="h3">
                    {props.text}
                </Typography>
                </div>
            </a >
        )
    }
}
