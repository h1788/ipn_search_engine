import {BACKEND_URL} from "../utils/backend"

export default function scrape(url, callback) {
    fetch(BACKEND_URL + '/scrape', {
        method: 'POST',
        headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "url": url
        })
    })
    .then(res => res.json())
    .then(res => {
        callback(res);
    });
}