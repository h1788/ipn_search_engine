import {BACKEND_URL} from "../utils/backend"

export default function popular(count, callback) {
    fetch(BACKEND_URL + '/popular', {
        method: 'POST',
        headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "count": count
        })
    })
    .then(res => res.json())
    .then(res => callback(res));
}