import {BACKEND_URL} from "../utils/backend"

export default function image_search(query, callback) {
    fetch(BACKEND_URL + '/search_graphic', {
        method: 'POST',
        headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "query": query
        })
    })
    .then(response => response.json())
    .then(response => {
        callback(response);
    });
}