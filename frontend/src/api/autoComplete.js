import {BACKEND_URL} from "../utils/backend"

export default function autocomplete(query, callback) {
    fetch(BACKEND_URL + '/autocomplete', {
        method: 'POST',
        headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "query": query
        })
    })
    .then(response => response.json())
    .then(data => {
        callback(data)
    })
}