import {BACKEND_URL} from "../utils/backend"

export default function related(query, callback) {
    fetch(BACKEND_URL + '/related', {
        method: 'POST',
        headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "query": query
        })
    })
    .then(res => res.json())
    .then(res => callback(res));
}