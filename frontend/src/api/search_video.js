import {BACKEND_URL} from "../utils/backend"

export default function search_video(query, callback) {
    fetch(BACKEND_URL + '/search_video', {
        method: 'POST',
        headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "query": query
        })
    })
    .then(res => res.json())
    .then(res => {
        callback(res);
    });
}