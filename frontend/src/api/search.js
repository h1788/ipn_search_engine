import { BACKEND_URL } from "../utils/backend"

export default function search(query, callback, page = 1, type = "") {
    if (type === "") {
        fetch(BACKEND_URL + '/search', {
            method: 'POST',
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "query": query,
                "page": page,
            })
        })
            .then(response => response.json())
            .then(json => callback(json))
    } else {
        fetch(BACKEND_URL + '/search', {
            method: 'POST',
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "query": query,
                "page": page,
                "type": type
            })
        })
        .then(response => response.json())
        .then(json => callback(json))
    }
}