import logo from './logo.svg';
import './App.css';
import IPNAppBar from './components/Appbar/appbar';
import Jumbo from './components/Jumbo/Jumbo';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Recomendations from './components/Recomendations/Recomendations';
import LandingPage from './components/LandingPage/LandingPage';
import SearchResultPage from './components/SearchResultPage/SearchResultPage';
import ArticlePage from './components/ArticlePage/ArticlePage';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import Dashboard from './components/dashboard/dashboard';
import Login from './components/login/login';
import { useLocalStorage } from 'react-use';

function App() {
  const theme = createTheme({
    palette: {
      primary: {
        main: '#1f3a57',
      },
    },
  });

  let [lang, setLang] = useLocalStorage('lang', 'pl');
  let [logged, setLogged] = useLocalStorage("logged", false);

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <div className="App">
          <Routes>
            <Route path="/login" element={<Login logged={logged} setLogged={setLogged} lang={lang} setLang={setLang}/>} />
            <Route path="/dashboard" element={<Dashboard logged={logged} setLogged={setLogged} lang={lang} setLang={setLang}/>} />
            <Route path="/search" element={<SearchResultPage logged={logged} setLogged={setLogged} lang={lang} setLang={setLang}/>} />
            <Route path="/article" element={<ArticlePage logged={logged} setLogged={setLogged} lang={lang} setLang={setLang}/>} />
            <Route path="/" element={<LandingPage logged={logged} setLogged={setLogged} lang={lang} setLang={setLang}/>} />
          </Routes>
        </div>
      </Router>
    </ThemeProvider>
  );
}

export default App;
