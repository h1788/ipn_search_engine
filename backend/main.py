from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.gapi import GoogleAPI
from src.util import scrape, related_search, autocomplete, search_video, popular, personal
from requests_api import SearchRequest, TrafilaturaRequest, PopularRequest
from fastapi.responses import RedirectResponse
app = FastAPI()
gapi = GoogleAPI()


origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def read_root():
    return RedirectResponse('/docs')


@app.post("/search")
async def search(request: SearchRequest):
    return gapi.search(request.query, request.page, request.type)


@app.post("/search_graphic")
async def search_graphic(request: SearchRequest):
    return gapi.search_graphic(request.query)


@app.post("/search_video")
async def search_video_youtube(request: SearchRequest):
    return search_video(request.query)


@app.post("/scrape")
async def scrape_trafilatura(request: TrafilaturaRequest):
    return scrape(request.url)


@app.post("/related")
async def related(request: SearchRequest):
    return related_search(request.query)


@app.post("/popular")
async def popular_random(request: PopularRequest):
    return popular(request.count)


@app.post("/personal")
async def personal_random(request: PopularRequest):
    return personal(request.count)


@app.post("/autocomplete")
async def autocomplete_duckduckgo(request: SearchRequest):
    return autocomplete(request.query)
