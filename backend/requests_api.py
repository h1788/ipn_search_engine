from typing import List, Literal, Optional

from pydantic import BaseModel


class SearchItem(BaseModel):
    type: str
    title: str
    text: str
    image: str
    link: str
    site_name: str
    site_domain: str
    author: str

    class Config:
        schema_extra = {
            "example": {'type': 'site',
                        'title': 'Sojusz Piłsudski–Petlura w kontekście politycznej i militarnej walki o kształt Europy Środkowej i Wschodniej',
                        'text': '24 mar 2021 ... Sojusz Piłsudski–Petlura w kontekście politycznej i militarnej walki o kształt Europy Środkowej i Wschodniej - Książki -',
                        'image': 'https://ipn.gov.pl/dokumenty/zalaczniki/1/oryginal/1-565159.jpg',
                        'link': 'https://ipn.gov.pl/pl/publikacje/ksiazki/140443,Sojusz-PilsudskiPetlura-w-kontekscie-politycznej-i-militarnej-walki-o-ksztalt-Eu.html',
                        'site_name': 'Sojusz Piłsudski–Petlura w kontekście politycznej i militarnej walki o kształt Europy Środkowej i Wschodniej',
                        'site_domain': 'ipn.gov.pl',
                        'author': 'Instytut Pamięci Narodowej',
                        'total_results': '4,600,000'
                        }
        }


class SearchGraphicItem(BaseModel):
    title: str
    context_link: str
    image: str
    link: str
    site_domain: str
    fileFormat: str


class VideoItem(BaseModel):
    id: str
    title: str
    thumbnail_url: str
    description: str
    channel: str
    url: str
    src: str


class SearchGraphicResponse(BaseModel):
    query: str
    total_results: str
    items: List[SearchGraphicItem]


class SearchVideoResponse(BaseModel):
    query: str
    items: List[VideoItem]


class ReletedResponse(BaseModel):
    query: str
    related_queries: List[str]


class SearchRequest(BaseModel):
    query: str
    page: Optional[int]
    type: Optional[Literal['site', 'pdf']]

    class Config:
        schema_extra = {
            "example": {'query': 'Piłsudski'}
        }


class SearchResponse(BaseModel):
    query: str
    total_results: str
    items: List[SearchItem]


class TrafilaturaRequest(BaseModel):
    url: str

    class Config:
        schema_extra = {
            "example": {'url': 'https://ipn.gov.pl/pl/publikacje/ksiazki/140443,Sojusz-PilsudskiPetlura-w-kontekscie-politycznej-i-militarnej-walki-o-ksztalt-Eu.html'}
        }


class TrafilaturaResponse(BaseModel):
    title: str
    author: str
    date: str
    tags: List[str]
    raw_text: str
    text: List[str]


class PopularRequest(BaseModel):
    count: int

    class Config:
        schema_extra = {
            "example": {'count': 5}
        }
