import json
from src.gapi import GoogleAPI

gapi = GoogleAPI()

# popular
queries = ['rewolta grudniowa', 'Rudolf Weigl', 'Marian Rejewski', 'Stanisław Marcin Ulam', 'łupaszka']

items = []
for query in queries:
    items += gapi.search(query, page=None, type='site').items

items = [e.dict() for e in items]

print(len(items))

with open('data/popular.json', 'w', encoding='utf-8') as f:
    json.dump(items, f, ensure_ascii=False, indent=4)


# personalized

queries = ['Armia Krajowa', 'powstanie warszawskie', 'Polskie Państwo Podziemne']

items = []
for query in queries:
    items += gapi.search(query, page=None, type='site').items

items = [e.dict() for e in items]

print(len(items))

with open('data/personalized.json', 'w', encoding='utf-8') as f:
    json.dump(items, f, ensure_ascii=False, indent=4)