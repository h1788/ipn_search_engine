import json
import random

import requests
import trafilatura
from requests_api import ReletedResponse, SearchResponse, TrafilaturaResponse, VideoItem, SearchVideoResponse
from youtubesearchpython import VideosSearch

remove_list = [
    "Pliki do pobrania",
    "Gdzie kupić publikacje:",
    "KsięgarnieKsięgarnia internetowaOddziały IPN Sprzedaż wysyłkowaDostępność Biuletynu IPN w placówkach Poczty Polskiej"
]

bing_headers = {
    'authority': 'api.bing.com',
}

duck_headers = {
    'authority': 'duckduckgo.com',
}

with open('data/popular.json', 'r', encoding='utf-8') as json_file:
    popular_items = json.load(json_file)
with open('data/personalized.json', 'r', encoding='utf-8') as json_file:
    personal_items = json.load(json_file)


def scrape(url):
    downloaded = trafilatura.fetch_url(url)
    e = json.loads(trafilatura.extract(downloaded, output_format='json'))
    txt = e['text']
    for remove_text in remove_list:
        txt = txt.replace(remove_text, ' ')
    print(e)
    return TrafilaturaResponse(
        title=e.get('title') if e.get('title') else '',
        author=e.get('author') if e.get('author') else '',
        date=e.get('date') if e.get('date') else '',
        tags=[t.strip() for t in e['tags'].split(',')],
        raw_text=e.get('raw-text') if e.get('raw-text') else '',
        text=[t.strip() for t in txt.split('\n') if t.strip() != '']
    )


def related_search(query):
    params = (
        ('query', query),
        ('Market', 'pl-PL')
    )
    response = requests.get(
        'https://api.bing.com/osjson.aspx', headers=bing_headers, params=params)
    q, r = response.json()
    return ReletedResponse(query=q, related_queries=r)


def autocomplete(query):
    params = (
        ('q', query),
        ('kl', 'pl-pl'),
    )
    response = requests.get(
        'https://duckduckgo.com/ac/', headers=duck_headers, params=params)
    r = [e['phrase'] for e in response.json()]
    return r


def search_video(query):
    videosSearch = VideosSearch(
        query + ' IPNtvPL', language='pl', region='PL', limit=20)
    res = videosSearch.result()['result']
    items = []
    for e in res:
        channel = e.get('channel').get('name', '')
        if channel == 'IPNtvPL':
            if not e.get('thumbnails'):
                e['thumbnails'] = [{}] 
            if not e.get('descriptionSnippet'):
                e['descriptionSnippet'] = [{}]
            items.append(
                VideoItem(
                    id=e.get('id', ''),
                    title=e.get('title', ''),
                    thumbnail_url=e.get('thumbnails')[0].get('url', ''),
                    description=e.get('descriptionSnippet')[0].get('text', ''),
                    channel=channel,
                    url=e.get('link', ''),
                    src=f'https://www.youtube.com/embed/{e.get("id")}'
                )
            )
    return SearchVideoResponse(query=query, items=items)


def popular(count):
    random.shuffle(popular_items)
    return SearchResponse(query='', items=popular_items[:count], total_results='')

def personal(count):
    random.shuffle(personal_items)
    return SearchResponse(query='', items=personal_items[:count], total_results='')
