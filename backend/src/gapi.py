from ast import Return
import imp
import os
import re
from urllib.parse import urlparse

from dotenv import load_dotenv
from googleapiclient.discovery import build
from requests_api import (SearchGraphicItem, SearchGraphicResponse, SearchItem,
                          SearchResponse)
from src.synonyms import replace_synonyms
load_dotenv()

dot_print = re.compile(re.escape('print'), re.IGNORECASE)


class GoogleAPI:
    def __init__(self):
        self.service = build("customsearch", "v1",
                             developerKey=os.environ.get('gapi_key'))
        self.search_engine_id = os.environ.get('search_engine_id')

    def search(self, query, page, type):
        query = dot_print.sub('', query)
        if not query:
            return SearchGraphicResponse(query=query, items=[], total_results='')
        query = replace_synonyms(query)
        if type == 'page':
            query += ' -inurl:pdf'
        elif type == 'pdf':
            query += ' inurl:pdf'

        page = 1 if not page else page
        res = self.service.cse().list(
            q=query,
            cx=self.search_engine_id,
            start=(page-1)*10 + 1
        ).execute()

        total_results = res['searchInformation'].get('formattedTotalResults')

        items = []
        if not res.get('items'):
            res['items'] = []
        for e in res['items']:
            type_google = 'site'
            fileFormat = e.get('fileFormat')
            if fileFormat:
                type_google = 'pdf' if fileFormat == 'PDF/Adobe Acrobat' else 'other'
            if not(type_google == 'pdf' and type == 'site'):
                if 'pagemap' not in e:
                    e['pagemap'] = {}
                if 'metatags' not in e['pagemap']:
                    e['pagemap']['metatags'] = [{}]
                items.append(
                    SearchItem(
                        type=type_google,
                        title=e['pagemap']['metatags'][0].get('og:title', ''),
                        text=e['snippet'],
                        image=e['pagemap']['metatags'][0].get('og:image', ''),
                        link=e['link'],
                        site_name=e['pagemap']['metatags'][0].get('og:title', ''),
                        site_domain=urlparse(e['link']).netloc,
                        author=e['pagemap']['metatags'][0].get('author', ''),
                    ))
        return SearchResponse(query=query, items=items, total_results=total_results)

    def search_graphic(self, query):
        if not query:
            return SearchGraphicResponse(query=query, items=[], total_results='')
        query = replace_synonyms(query)
        res = self.service.cse().list(
            q=query,
            cx=self.search_engine_id,
            searchType='image',
            filter='1',
        ).execute()
        total_results = res['searchInformation'].get('formattedTotalResults')

        items = []
        if not res.get('items'):
            res['items'] = []
        # add above 10
        if len(res.get('items')) == 10:
            res_extra = self.service.cse().list(
                q=query,
                cx=self.search_engine_id,
                searchType='image',
                filter='1',
                start=11
            ).execute()
            if res_extra.get('items'):
                res['items'] += res_extra['items']

        for e in res['items']:
            items.append(
                SearchGraphicItem(
                    type='graphic',
                    title=e.get('title'),
                    context_link=e['image'].get('contextLink'),
                    image=e.get('htmlTitle'),
                    link=e.get('link'),
                    site_domain=e.get('displayLink'),
                    fileFormat=e.get('fileFormat')
                ))
        return SearchGraphicResponse(query=query, items=items, total_results=total_results)
